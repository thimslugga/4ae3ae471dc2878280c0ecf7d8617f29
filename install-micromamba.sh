#!/bin/bash

# https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html

: '
if [ -z $MAMBA_EXE ]; then
  echo "MAMBA_EXE is unset. Install MAMBA first!"
  exit
fi
'

#wget -qO- https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba --strip-components=1
curl -sSfL 'https://micromamba.snakepit.net/api/micromamba/osx-64/latest' -O | tar -xvj bin/micromamba --strip-components=1

./micromamba shell init -s bash -p ~/micromamba
source ~/.bashrc

#micromamba activate
#micromamba install python=3.8 -c conda-forge
#micromamba install python=3.8 jupyter -c conda-forge
# micromamba create -p /some/new/prefix xtensor -c conda-forge

#micromamba self-update